//Xhook
xhook.after(function (request, response, cb) {
    response.text().then(function (text) {
        if (text == 'error') {
            location.reload();
        }
        cb(new Response(text));
    });
});

//WebSocket
haveSocket = false;
let socket;


//Vue
const store = {
    data: {
        pauseGame: false,
        over: false,
        overMessage: '',
        playerScore: 0,
        ememyScore: 0,
        room: 0,
        playerCards: [],
        enemyCards: [],
        responseSend: true,
        myTurn: false,
    },
    async startGame() {
        this.data.pauseGame = false
    },
    async checkLocale() { // Проверяет локал стордж и либо создает новую игру либо подгружает и востанавливает страую исходя из локал сторджа.
        locale = localStorage.getItem('locale')
        if (locale == null) {
            this.data.pauseGame = true
            req = await fetch(reloadGameUrl + '?room=' + store.data.room);
            res = await req.json();
            locale = {
                'room': res.room,
                'over': res.over,
                'playerKey': res.playerKey,
            }
            this.data.myTurn = true;
            this.data.room = res.room;
            localStorage.setItem('locale', JSON.stringify(locale))
            if (!haveSocket) {
                this.createSocket(this.data.room)
            }
        } else {
            try {
                req = await fetch(loadGameUrl + '?room=' + JSON.parse(locale).room + '&player=' + JSON.parse(locale).playerKey);
                res = await req.json();

                locale = localStorage.getItem('locale')
                this.data.room = JSON.parse(locale).room;
                this.data.over = res.over;
                this.data.myTurn = res.playerTurn;
                this.data.playerCards = res.playerCards;
                this.data.playerScore = res.playerScore;
                this.data.enemyScore = res.enemyScore;
                cards = [];
                if (store.data.over == false) {
                    for (let i = 0; i < res.enemyCardsCol; i++) {
                        cards.push("../static/img/cardBack.jpg");
                        this.data.enemyCards = cards;
                    }
                } else {
                    this.data.enemyCards = res.enemyCards
                }
                if (!haveSocket) {
                    this.createSocket(this.data.room)
                }
            } catch {
                alert(`Ваша комната была удалена, возможо вашему сопернику надоело проигрывать и он вышел, будет создана новая комната с бесчувственным и невозмутимым ботом.\nНу или просто напросто ваши данные устарели.`);
                localStorage.clear();
                this.checkLocale();
                location.reload();
            }
        }
    },
    async reloadGame() { //Перезапускает игру, если игра с ботом и первый ход достается ему то берет кару и передает ход игроку.
        if (this.data.responseSend == true) {
            this.data.responseSend = false;
            locale = localStorage.getItem('locale')
            req = await fetch(reloadGameUrl + '?room=' + JSON.parse(locale).room + '&player=' + JSON.parse(locale).playerKey);
            try {
                res = await req.json();
                this.data.room = res.room;
                this.data.enemyCards = [];
                this.data.playerCards = [];
                this.data.playerScore = 0;
                this.data.enemyScore = 0;
                this.data.overMessage = '';
                this.data.over = false;
                this.data.myTurn = res.myTurn;
                locale = JSON.parse(locale);
                locale.over = false;
                localStorage.setItem('locale', JSON.stringify(locale));
                if (this.data.myTurn == false && res.bot) {
                    cards = [];
                    req = await fetch(getBotCardUrl + '?room=' + this.data.room + '&player=' + locale.playerKey);
                    res = await req.json();
                    for (let i = 0; i < res.enemyCardsCol; i++) {
                        cards.push("../static/img/cardBack.jpg");
                    }
                    this.data.enemyCards = cards;
                    this.data.myTurn = true
                }
                if (!res.bot) {
                    socket.send(JSON.stringify({
                        message: 'Restart'
                    }))
                }
            } catch {}
            this.data.responseSend = true;
        }
    },
    async getCard() { // Добирает карту игроку и боту если такой есть в комнате попутно проверяя не закончилась ли игра.
        if (this.data.responseSend == true) {
            this.data.responseSend = false;
            locale = localStorage.getItem('locale')
            req = await fetch(getCardUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
            res = await req.json();
            let enemyBot = res.enemyBot;
            this.data.playerCards.push(res.cardImg);
            this.data.myTurn = res.myTurn;
            await this.checkOver()
            if (!this.data.over && enemyBot) {
                cards = [];
                req = await fetch(getBotCardUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
                res = await req.json();
                for (let i = 0; i < res.enemyCardsCol; i++) {
                    cards.push("../static/img/cardBack.jpg");
                }
                this.data.enemyCards = cards;
                this.checkOver()
                this.data.myTurn = true
            } else {
                socket.send(JSON.stringify({
                    message: 'Next'
                }))
            }
            this.data.responseSend = true;
        }
    },
    async checkOver() { // Проверяет не закончилась ли игра и если закончилась то раскрывает карты и выводит сообщение.
        locale = localStorage.getItem('locale')
        req = await fetch(overCheckUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
        res = await req.json();
        this.data.playerScore = res.playerScore;
        this.data.over = res.over;
        this.data.overMessage = res.message;
        if (this.data.over == true) {
            this.data.enemyScore = res.enemyScore;
            this.data.enemyCards = res.enemyCards;
            locale = JSON.parse(locale);
            locale.over = true;
            locale = JSON.stringify(locale);
            localStorage.setItem('locale', locale);
        }
    },
    async overGame() { // Заканчивает ход игрока и проверяет закончилась ли игра, после при наличии бота добирает ему карыт и проверяет на конец игры. 
        if (this.data.responseSend == true) {
            this.data.responseSend = false;
            locale = localStorage.getItem('locale')
            req = await fetch(overGameUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
            res = await req.json();
            this.data.myTurn = false;
            if (res.gameOver) {
                this.checkOver()
            }

            if (!this.data.over && res.enemyBot) {
                req = await fetch(getBotCardUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
                res = await req.json();
                while (this.data.enemyCards.length < res.enemyCardsCol) {
                    this.data.enemyCards.push("../static/img/cardBack.jpg");
                    req = await fetch(getBotCardUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey);
                    res = await req.json();
                }
            } else {
                socket.send(JSON.stringify({
                    message: 'Stop'
                }))
            }
            this.checkOver()
            this.data.responseSend = true;
        }
    },
    async showJoin() { // Показывает окно присоединения и ждет ввода номера комнаты, вызывает join
        let joinRoom = prompt(`Введите номер комнаты, к которой вы хотите присоединиться\nНомер вашей комнаты --> ${this.data.room}`)
        if (joinRoom == this.data.room) {
            alert("Это номер вашей комнаты")
            return false
        }else if(joinRoom != null){
            this.join(joinRoom)
        }
    },
    async join(joinRoom) { // Пробует подключить игрока к комнате если получается то обновляет локалстордж, сокет, и перезагружает игру. Если не получаается то сообщет об этом поользователю.
        locale = localStorage.getItem('locale');
        req = await fetch(joinUrl + '?room=' + store.data.room + '&player=' + JSON.parse(locale).playerKey + '&newRoom=' + joinRoom);
        res = await req.json();
        if (res.room == 0) {
            alert('Подключение не удалось')
        } else {
            socket.send(JSON.stringify({
                message: 'Out'
            }))
            socket.close();
            await fetch(outUrl + '?room=' +  JSON.parse(locale).room);
            locale = JSON.parse(locale);
            locale.room = res.room;
            this.data.room = res.room;
            locale.playerKey = res.newPlayerKey;
            locale = JSON.stringify(locale);
            localStorage.setItem('locale', locale);
            await this.createSocket(this.data.room)
            await this.reloadGame()
            socket.send(JSON.stringify({
                message: 'Next'
            }))
            alert('Вы успешно подключились')
        }
    },
    async createSocket(room) { //Создает вебсокет по номеру комнаты который передается в качестве аргумента. Создает обработчики событий для сокета.
        //WebSocket
        haveSocket = true;
        socket = await new WebSocket(`ws://${location.host}/ws/${room}`)

        socket.onmessage = function (e) {
            let res = JSON.parse(e.data)
            if (res.message == 'Next') {
                store.checkLocale()
                store.data.pauseGame = false
                store.checkOver()
            } else if (res.message == 'Restart') {
                store.checkLocale()
                store.data.enemyCards = []
            } else if (res.message == 'Stop') {
                store.checkLocale()
                store.checkOver()
            } else if('Out'){
                store.checkLocale();
            }
        }

        socket.onclose = function (e) {
            console.error('Вебсокет умер(');
        }
    },
    async out(){ // Отправляет сообщение об отключении по сокету и удаляет комнату в которой находится после этого перезагружает страницу.
        locale = localStorage.getItem('locale');
        socket.send(JSON.stringify({
            message: 'Out'
        }))
        socket.close();
        await fetch(outUrl + '?room=' +  JSON.parse(locale).room);
        await localStorage.clear();
        location.reload();
    }
}


store.checkLocale();


let player = new Vue({
    el: '#player',
    data: () => ({
        importData: store,
    }),
})


let enemy = new Vue({
    el: '#enemy',
    data: () => ({
        importData: store,
    }),
})