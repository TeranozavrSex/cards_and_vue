from django.urls import path
from .consumers import WSConsumer

ws_urlpatterns = [
    path('ws/<room>',WSConsumer.as_asgi()),
]