from email.mime import image
from django.db import models

# Create your models here.

class Game(models.Model):
    deck_id = models.CharField(max_length=25)
    over = models.BooleanField(default=False)

    @classmethod
    def create(cls,_deck_id):
        game = cls(deck_id = _deck_id)
        return game

class Player(models.Model):
    score = models.IntegerField(default=0)
    key = models.CharField(max_length=100)
    bot = models.BooleanField()
    turn = models.BooleanField()
    over = models.BooleanField()
    game = models.ForeignKey(Game ,null=True, blank=True, on_delete=models.CASCADE)

    @classmethod
    def create(cls, _turn, _over, _bot, _key, _game):
        player = cls( turn = _turn, over = _over, bot = _bot,key = _key, game = _game)
        return player


class Card(models.Model):
    value = models.CharField(max_length=10)
    image = models.ImageField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    
    @classmethod
    def create(cls,_value,_image, _player):
        card = cls(value = _value, image = _image, player = _player)
        return card

