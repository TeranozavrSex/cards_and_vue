from cgitb import text
from dataclasses import dataclass
from email import message
import json
from re import S
from aiohttp import request

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.conf import settings

class WSConsumer (WebsocketConsumer):
    def connect(self):
        async_to_sync(self.channel_layer.group_add)("game", self.channel_name)
        self.accept()



    def receive(self, text_data):
            message = json.loads(text_data)['message']
            async_to_sync(self.channel_layer.group_send)(
            "game",
            {
                'type': 'turn_message',
                "message": message,
            },)


    def disconect(self):
        async_to_sync(self.channel_layer.group_discard)("game", self.channel_name)



    def turn_message(self, event):
        message = event['message']

        self.send(text_data=json.dumps({
            "message": message,
        }),)