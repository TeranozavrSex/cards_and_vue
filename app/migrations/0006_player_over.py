# Generated by Django 4.0.2 on 2022-02-09 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_player_turn'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='over',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
