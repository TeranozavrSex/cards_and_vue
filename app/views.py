from dataclasses import fields
from datetime import date
from email import header, message
from email.mime import image
from hashlib import new
from wsgiref import headers
from aiohttp import request
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse, QueryDict
from django.shortcuts import render
import json
import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from app.models import Card, Game, Player
from django.core import serializers
from django.db.models import Q
import uuid
from random import randint

valueTab = {
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        '10': 10,
        'JACK': 2,
        'QUEEN': 3,
        'KING': 4,
        'ACE': 11,
    }

# Рендер странички
def face(request):
    return render(request, 'app/face.html')


# Получает комнату, если она нулевая то создает новую, 
# если она существует и в ней закончена игра,
# то перезапускает ее путем зануления всего.
def reloadGame(request): 
    if(request.GET.get('room') ==  '0'):
        response = requests.get('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
        deck_id = json.loads(response.text).get('deck_id')
        game = Game.create(deck_id)
        game.save()
        player1 = Player.create(True,False,False,str(uuid.uuid4()), game)
        player1.save()
        player2 = Player.create(False,False,True, str(uuid.uuid4()), game)
        player2.save()
        data = {'over':game.over ,'room' : game.pk, 'playerKey' : player1.key,  'bot' : True}
        return JsonResponse(data)
    else:
        room = request.GET.get('room')
        game = Game.objects.get(pk = room)
        playerKey = request.GET.get('player')
        player = Player.objects.get(key = playerKey)
        enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
        if(game.over == True or player.score == 0 and enemy.score == 0):
            response = requests.get('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
            deck_id = json.loads(response.text).get('deck_id')
            game.deck_id = deck_id
            game.over = False
            player = Player.objects.get(key = playerKey)
            enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
            Card.objects.filter(Q(player  = player) | Q(player  = enemy)).delete()
            enemy.score = 0
            player.score = 0
            if(randint(1,2) == 1):
                player.turn = True
                enemy.turn = False
            else:
                player.turn = False
                enemy.turn = True
            enemy.over = False
            player.over = False
            enemy.save()
            player.save()
            game.save()
            if(player.bot == True or enemy.bot == True):
                bot = True
            else:
                bot = False
            data = {'over' : game.over, 'room' : game.pk, 'myTurn': player.turn, 'bot' : bot }
            return JsonResponse(data)
        else:
            return HttpResponse('error')


# Получает комнату и игрока проверяет что игрок пренадлежит этой комнате.
#  Возвращает словарь с картой и счетом игрока.
def getCard(request):
    room = request.GET.get('room')
    playerKey = request.GET.get('player')
    game = Game.objects.get(pk = room)
    player = Player.objects.get(key = playerKey)
    enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
    if(game.over != True and player.turn == True and player.game == game):
        cardResponse = requests.get('https://deckofcardsapi.com/api/deck/'+ game.deck_id +'/draw/?count=1')
        cardResponse = json.loads(cardResponse.text)
        cardValue =  valueTab.get(cardResponse.get('cards')[0].get('value'))
        player.score = player.score + cardValue
        card = Card.create(cardValue, cardResponse.get('cards')[0].get('image'), player)
        card.save()
        if(enemy.over == False):
            player.turn = False
            enemy.turn = True 
            enemy.save()
        player.save()
        data = { 
            'cardImg': card.image.name, 
            'score': player.score, 
            'enemyBot': enemy.bot,
            'myTurn' : player.turn
        }
        return JsonResponse(data)
    else:
        return HttpResponse('error')


# Получает комнату и игрока проверяет что игрок пренадлежит этой комнате, 
#  проверяет бот ли игрок.
#  Берет боту карту.
#  Проверяет руку бота на тузы при переборе и возвращает 
#  колличество карт в руке у бота.
#  Если игрок сказал стоп то не заканчивает ход.
#  Если игрок не говорил стоп то отбирает ход у бота и передет игроку.
def getBotCard(request):
    room = request.GET.get('room')
    playerKey = request.GET.get('player')
    game = Game.objects.get(pk = room)
    player = Player.objects.get(key = playerKey)
    bot = Player.objects.get(~Q(key = playerKey) & Q(game = game))
    if(game.over != True and bot.game == game and bot.bot == True):
        bot.turn = True
        bot.save()
        if (bot.score < 18):
            cardResponse = requests.get('https://deckofcardsapi.com/api/deck/'+ game.deck_id +'/draw/?count=1')
            cardResponse = json.loads(cardResponse.text)
            cardValue = valueTab.get(cardResponse.get('cards')[0].get('value'))
            bot.score = bot.score + cardValue
            card = Card.create(cardValue, cardResponse.get('cards')[0].get('image'), bot)
            card.save()
            bot.save()
            if(bot.score > 21):
                try:
                    card = Card.objects.filter(bot = bot.id, value = 11).first()
                    card.value = 1
                    bot.score = bot.score - 10
                    bot.save()
                    card.save()
                except:
                    pass
        else:
            bot.over = True
            bot.save()
        enemyCardsCol =  len(Card.objects.filter(player = bot.id).values('image'))
        if(player.over == False):
            bot.turn = False
            player.turn = True
            bot.save()
            player.save()
        data = {
            'enemyCardsCol' : enemyCardsCol,
            'botOver' : bot.over
        }
        return  JsonResponse(data)
    else:
        return HttpResponse('error')


#Заканчивает ход игрока и ставит флаг игрока over в фолс.
def overGame(request):
    room = request.GET.get('room')
    playerKey = request.GET.get('player')
    game = Game.objects.get(pk = room)
    player = Player.objects.get(key = playerKey)
    enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
    if(game.over != True and player.turn == True and player.game == game):    
        player.over = True
        player.turn = False
        enemy.turn = True
        enemy.save()
        player.save()
        data = {
            'gameOver' : False,
            'enemyBot': enemy.bot,
        }
        if(enemy.over == True):
            data['gameOver'] = True
        return JsonResponse(data)
    else:
        return HttpResponse('error') 


# Получает экземпляр игрока и его противника проверяет 
# не проигрл ли кто по правилам игры. После проверки заканчивает ход игрока 
# и отправляет данные.
def overCheck(request):
    room = request.GET.get('room')
    game = Game.objects.get(pk = room)
    playerKey = request.GET.get('player')
    player = Player.objects.get(key = playerKey)
    enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
    enemyCardsCol =  len(Card.objects.filter(player = enemy.id).values('image'))
    data ={
        'over': False,
        'message' : '',
        'enemyCards' : [],
        'enemyScore' : 0,
        'playerScore' : player.score,
        'enemyCardsCol' : enemyCardsCol,
    }
    if(player.score > 21 or enemy.score > 21):
        if(player.score > 21):
            try:
                card = Card.objects.filter(player = player.id, value = 11).first()
                card.value = 1
                player.score = player.score - 10
                card.save()
                player.save()
                data['playerScore'] =  player.score
            except:
                data['message'] = 'Вы проиграли!' 
                data['over'] = True
                game.over = True;
                game.save()
        if(enemy.score > 21):
            try:
                card = Card.objects.filter(player = enemy.id, value = 11).first()
                card.value = 1
                enemy.score = enemy.score - 10
                card.save()
                enemy.save()
                data['enemyScore'] =  enemy.score
            except:
                data['message'] = 'Вы выиграли!' 
                data['over'] = True
                game.over = True;
                game.save()
    if(player.score == 21 or enemy.score == 21):
        if(player.score == 21):
            data['message'] = 'Вы выиграли!' 
            data['over'] = True
            game.over = True;
            game.save()
        if(enemy.score == 21):
            data['message'] = 'Вы проиграли!' 
            data['over'] = True
            game.over = True;
            game.save()
    if(player.over and enemy.over):
        if(player.score > 21):
            data['message'] = 'Вы проиграли!' 
            data['over'] = True
            game.over = True;
            game.save()
        elif(enemy.score > 21):
            data['message'] = 'Вы выиграли!' 
            data['over'] = True
            game.over = True;
            game.save() 
        elif(player.score > enemy.score):
            data['message'] = 'Вы выиграли!' 
            data['over'] = True
            game.over = True;
            game.save() 
        elif(player.score < enemy.score):
            data['message'] = 'Вы проиграли!' 
            data['over'] = True
            game.over = True;
            game.save()
        elif(player.score == enemy.score):
            data['message'] = 'Ого, ничья)!' 
            data['over'] = True
            game.over = True;
            game.save()
    if(data.get('message') != ""):
        for i in range(0, enemyCardsCol):
            data.get('enemyCards').append(Card.objects.filter(player = enemy.id).values('image')[i].get('image'))
            data['enemyScore'] = enemy.score
    return JsonResponse(data)


# Получает номер комнаты и ключ игрока, 
# проверяет есть ли такой игрок в этой комнате 
# и отправляет ему данные о существующей игре. Нужные для ее возобновления.
def loadGame(request):
    room = request.GET.get('room')
    playerKey = request.GET.get('player')
    game = Game.objects.get(pk = room)
    player = Player.objects.get(key = playerKey)
    enemy = Player.objects.get(~Q(key = playerKey) & Q(game = game))
    playerCards = []
    enemyCards = []
    playerCardsQDict = Card.objects.filter(player = player.id).values('image')
    enemyCardsCol =  len(Card.objects.filter(player = enemy.id).values('image'))
    for item in playerCardsQDict:
        playerCards.append(item.get('image'))
    loadData = {
        'playerCards' : playerCards,
        'playerScore' : player.score,
        'playerTurn' : player.turn,
        'enemyScore' : 0,
        'enemyCardsCol' : enemyCardsCol,
        'enemyCards' : enemyCards,
        'over': game.over,
        'enemyBot' : enemy.bot,
    }
    if(game.over == True):
        for i in range(0, enemyCardsCol):
            loadData.get('enemyCards').append(Card.objects.filter(player = enemy.id).values('image')[i].get('image'))
            loadData['enemyScore'] = enemy.score
    return JsonResponse(loadData)


# Получает номер комнаты в которую пользователь хочет присоедениться,
# проверяет существование этой комнаты в бд.
# Проверяет есть ли в ней бот, то есть свободное для игрока место.
# Возвращает тру или фолс
def checkJoin(roomKey):
    try:
        newGame = Game.objects.get(pk = roomKey)
        data = Player.objects.get(Q(game = newGame) & Q(bot = True))
    except:
        data = False
        data = Player.objects.filter(game = newGame)
    player = Player.objects.get(Q(game = newGame) & Q(bot = False))
    if(data != False and newGame.over == False and player.score > 0):
        data = False
    return data


# Вызывает функцию проверки можно ли присоедениться к комнате.
# И если можно то отправляет на клиента номер комнаты, если нет то отправляет ноль.
def join(request):
    newRoomNum = request.GET.get('newRoom')
    playerKey = request.GET.get('player')
    player = Player.objects.get(key = playerKey)
    data = {
            'room': 0,
            'newPlayerKey': '',
        }
    try:
        freePlace = checkJoin(newRoomNum)
        if(freePlace != False):
            player.delete()
            data['room'] = newRoomNum
            data['newPlayerKey'] = freePlace.key
            freePlace.bot = False
            freePlace.save()
    except:
        data['room'] = 0
    return JsonResponse(data)


# Удаляет комнату по переданному номеру.
def out(request):
    room = request.GET.get('room')
    Game.objects.get(pk = room).delete()
    return HttpResponse('Delete')