from unicodedata import name
from django.urls import path
from django.urls.conf import include
from .views import *

urlpatterns = [
    path('',  face ),
    path('reloadGame/', reloadGame, name = 'reloadGame'),
    path('getCard/', getCard, name = 'getCard'),
    path('overGame/', overGame, name = 'overGame'),
    path('overCheck/', overCheck, name = 'overCheck'),
    path('getBotCard/', getBotCard, name = 'getBotCard'),
    path('loadGame/', loadGame, name = 'loadGame'),
    path('checkJoin/', checkJoin, name = 'checkJoin'),
    path('join/', join, name = 'join'),
    path('out/', out, name = 'out'),
]
